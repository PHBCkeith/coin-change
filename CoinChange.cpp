/**
*
* My solution to the Coin Change challenge on hackerrank.com
*
* Copyright (C) 2018 by Keith Jordan <3keithjordan3@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
* 
**/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// optional-like template class.  It allows for determinination of whether or not the value
// has been set yet.  The bool operator provides a way to check this.  Once set, it cannot
// be unset.
template <class T>
class MyOptional {

	T value;
	bool isSet;

public:

	MyOptional() {
		isSet = false;
	}

	void setValue(T t) {
		value = t;
		isSet = true;
	}

	long getValue() {
		return value;
	}

	operator bool() {
		return (isSet);
	}

	void operator=(T otherT) {
		setValue(otherT);
	}

};

// get subsolution of ways to get targetValue with all coins from index i onward in coinDenominations.
// mapOfWays is used to memoize subsolutions.
long numWaysToGetTargetValue(int targetValue, int cdStartIndex, vector<int> &coinDenominations,
														 vector<vector<MyOptional<long>>> &mapOfWays) {

	// check if we've already have a subsolution for current targetvalue and cdStartIndex
	if (mapOfWays[targetValue][cdStartIndex])
		return mapOfWays[targetValue][cdStartIndex].getValue();

	if (targetValue == 0)
		return 1;

	long ways = 0;

	// for each coin from cdStartIndex onward, add the subsolution when subtracting the value of 
	// that coin. The loop iterates through coinDenominations because once we've found all solutions
	// including coin[i], we need to add all solutions including coin[i + 1] but not coin[i], then 
	// coin[i + 2] but not coin[i] or coin[i + 1] etc.
	for (int i = cdStartIndex; ((i < coinDenominations.size())&&
			(coinDenominations[i] <= targetValue)); ++i) {
			ways += numWaysToGetTargetValue(targetValue - coinDenominations[i], i,
																			coinDenominations, mapOfWays);
	}

	// memoize subsolution
	mapOfWays[targetValue][cdStartIndex] = ways;

	return ways;
}

int main() {

	// get target value and the number of different coin types
	int targetValue, numCoinDenominations;
	cin >> targetValue >> numCoinDenominations;
	// this vector stores the different coin denominations
	vector<int> coinDenominations(numCoinDenominations);

	for (int i = 0; i < numCoinDenominations; ++i) {
		cin >> coinDenominations[i];
	}
	// sort for more efficient traversal (no need to check next coin if current one is too big)
	sort(coinDenominations.begin(), coinDenominations.end());
	// 2D vector used to memoize subsolutions.  The first index is the subtarget value.
	// The second index is the starting index in the coinDenominations vector.
	// Example:
	//   if targetValue = 5, and coinDenominations = {1, 2, 3, 4, 5}
	//   mapOfWays[3][1] = subsolution for targetValue = 3 and coins {2, 3, 4, 5}
	//   mapOfWays[1][3] = subsolution for targetValue = 1 and coins {4, 5}
	vector<vector<MyOptional<long>>> mapOfWays(targetValue + 1,
	 						vector<MyOptional<long>>(numCoinDenominations, MyOptional<long>()));

	cout << numWaysToGetTargetValue(targetValue, 0, coinDenominations, mapOfWays) << "\n";

	return 0;
}