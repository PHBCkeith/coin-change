# Coin Change

This is my solution to the Coin Change problem on hackerrank.com

## Authors

* **Keith Jordan** - [contact](mailto:3keithjordan3@gmail.com)

## License

This project is licensed under the GNU General Public License - see the [COPYING](COPYING) file for details


